<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   */
  public function up(): void
  {
    Schema::create('rekam_medis', function (Blueprint $table) {
      $table->id();
      $table->string('id_user', 20);
      $table->string('id_pasien', 20);
      $table->string('id_penyakit', 20);
      $table->date('tanggal');
      $table->string('keluhan');
      $table->string('saran');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down(): void
  {
    Schema::dropIfExists('rekam_medis');
  }
};
