<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   */
  public function up(): void
  {
    Schema::create('pasiens', function (Blueprint $table) {
      $table->id();
      $table->string('id_pasien', 20);
      $table->string('nama', 50);
      $table->string('no_ktp', 16);
      $table->date('tanggal_lahir');
      $table->string('jenis_kelamin');
      $table->string('no_hp', 12);
      $table->string('alamat');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down(): void
  {
    Schema::dropIfExists('pasiens');
  }
};
